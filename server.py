# sudo pip3 install flask

from flask import Flask, request, jsonify
import mysql.connector

# create a server process
app = Flask(__name__)


def open_connection():
  connection = mysql.connector.connect(host="localhost", user="root", password="root", database="movies_db", port=3306)
  
  return connection


def execute_select_query(query):
  """
  this function is supposed to execute select query
  and return the output
  """
  connection = open_connection()
  
  cursor = connection.cursor()
  cursor.execute(query)

  data = cursor.fetchall()

  cursor.close()
  connection.close()
  
  return data


def execute_query(query):
  """
  this function is supposed to execute insert, update
  and delete queries and commit the connection
  """
  connection = open_connection()
  cursor = connection.cursor()
  cursor.execute(query)
  connection.commit()

  cursor.close()
  connection.close()


def create_response(data, error=None):
  # create empty dictionary
  result = dict()

  # check if there is any error
  if error == None:
    # there is no error to report
    result['status'] = 'success'
    result['data'] = data

  else:
    # there is an error 
    result['status'] = 'error'

  return jsonify(result)
    

@app.route("/", methods=["GET"])
def welcome():
  return "welcome to my IoT application"


@app.route("/movies", methods=["GET"])
def get_temperature_data():
  query = "select movies from movies"
  
  movies = execute_select_query(query)
  print(movies)

  movies_names = []
  for (movies) in movies:
    movies_names.append({
      "movies": movies,
    })

  return create_response(movies_names)


@app.route('/movies', methods=["POST"])
def post_temperature():
  # get the temperature value sent by NodeMCU
	movies = request.form.get('movies')
	query = f"insert into movies () values ({movies})"

    execute_query(query)
  return create_response('movie added')

# start the server
app.run(host="0.0.0.0", debug=True, port=4000)


