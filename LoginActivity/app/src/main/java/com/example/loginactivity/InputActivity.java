package com.example.loginactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class InputActivity extends AppCompatActivity {

    Button buttonSave,buttonCancel;
    EditText editMovieName,editGenre,editRating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);

        buttonSave= findViewById(R.id.buttonSave);
        buttonCancel = findViewById(R.id.buttonCancel);

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String movieName = editMovieName.getText().toString();
                String movieGenre = editGenre.getText().toString();
                String movieRating = editRating.getText().toString();


                // step 3: send the data
                Intent data = new Intent();
                data.putExtra("movieName", movieName);
                data.putExtra("movieGenre", movieGenre);
                data.putExtra("movieRating", movieRating);
                setResult(0, data);

                finish();

            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }
}