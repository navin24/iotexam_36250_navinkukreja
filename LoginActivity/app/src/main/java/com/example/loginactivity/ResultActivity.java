package com.example.loginactivity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class ResultActivity extends AppCompatActivity {

    ArrayList<Movies> movies = new ArrayList<>();
    ArrayAdapter<Movies> adapter;
    ListView listView;


    Button addButton,getButton,backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        addButton = findViewById(R.id.addButton);
        backButton = findViewById(R.id.backButton);
        getButton = findViewById(R.id.getButton);

        adapter = new ArrayAdapter<Movies>(this, android.R.layout.simple_list_item_1, movies);
        listView = findViewById(R.id.listView);
        listView.setAdapter(adapter);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResultActivity.this, InputActivity.class);
                // startActivity(intent);

                // step 1: change the intention
                startActivityForResult(intent, 0);
            }
        });

        getButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        String movie = data.getStringExtra("movieName");
        String genre = data.getStringExtra("movieGenre");
        String rating = data.getStringExtra("movieRating");


    }
}

